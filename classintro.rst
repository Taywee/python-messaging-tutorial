.. _classintro:

Class Intro
###########

Classes are a complex subject.  `The Python Classes reference`_ covers many
details, but it is also heavy in specific and historical language, and perhaps
not good for a beginner.  The easiest way of thinking about it is by comparison
to dictionaries.  You can encompass all your necessary data for a single thing
in a dictionary and operate on it using functions, if you like, but classes help
this by making the code that works with data accompany that data.  Classes also
enable you to avoid code repetition by grouping common behavior into subclasses.

For this page, and this page only, we will be using a simple text RPG style game
to build our concepts.

Dictionaries
============

The easiest way of grouping data (for instance, player name, hp, attack, and
defense) is with a dictionary.  In some programming languages (notably Lua and
old-style Javascript), this is the primary way of grouping data for use.

.. _generalbehavior:

General behavior using dictionaries
-----------------------------------

This is the dictionary setup of the common general behavior::

  def make_unit(name, hp, attack, defense):
      return {
          'name': name,
          'hp': hp,
          'attack': attack,
          'defense': defense,
      }

  def attack(attacker, defender):
      # Use "max" with 0 because otherwise we can deal negative damage
      damage = max((attacker['attack'] - defender['defense'], 0))
      defender['hp'] -= damage
      print(f'{attacker["name"]} attacks {defender["name"]} for {damage} damage, leaving with {defender["hp"]} health left')

  def is_dead(unit):
      return unit['hp'] <= 0

  player = make_unit(
      name='Player',
      hp=10,
      attack=4,
      defense=2,
  )

  enemies = [
      make_unit(
          name='Goblin',
          hp=5,
          attack=6,
          defense=1,
      ),
      make_unit(
          name='Bat',
          hp=5,
          attack=3,
          defense=0,
      ),
      make_unit(
          name='Snake',
          hp=5,
          attack=5,
          defense=1,
      ),
  ]

  while True:
      if is_dead(player):
          print(f'{player["name"]} has died.')
          break
      if not enemies:
          print(f'{player["name"]} has won.')
          break

      for enemy in enemies:
          attack(player, enemy)
          if is_dead(enemy):
              print(f'{enemy["name"]} has died.')
              enemies.remove(enemy)
          else:
              attack(enemy, player)

              if is_dead(player):
                  break

Which outputs:

.. code-block:: text
 
  Player attacks Goblin for 3 damage, leaving with 2 health left
  Goblin attacks Player for 4 damage, leaving with 6 health left
  Player attacks Bat for 4 damage, leaving with 1 health left
  Bat attacks Player for 1 damage, leaving with 5 health left
  Player attacks Snake for 3 damage, leaving with 2 health left
  Snake attacks Player for 3 damage, leaving with 2 health left
  Player attacks Goblin for 3 damage, leaving with -1 health left
  Goblin has died.
  Player attacks Snake for 3 damage, leaving with -1 health left
  Snake has died.
  Player attacks Bat for 4 damage, leaving with -3 health left
  Bat has died.
  Player has won.

This can entirely function!  This is pretty similar in many ways to how C
structures work, but it can also be quite unreadable and error-prone, and
doesn't provide any *type introspection* (ie. The ability to check what type
an object actually is).  You can also be in a pickle if you want to describe
behavior specific to a particular unit (like if you want the snake to have a
poisoning attack, or the bat to only be attackable with ranged weapons).

Type-specific behavior using names
----------------------------------

Let's consider the situation in which we want the bat only attackable with
ranged weapons.  We add the behavior by making make_unit optionally take in a
ranged_attack parameter and making attack take into account the defender's
name::

    def make_unit(name, hp, attack, defense, ranged_attack=None):
        return {
            'name': name,
            'hp': hp,
            'attack': attack,
            'ranged_attack': ranged_attack,
            'defense': defense,
        }

    def attack(attacker, defender):
        if defender['name'] == 'Bat':
            attack = attacker['ranged_attack']
        else:
            attack = attacker['attack']

        if attack is not None:
            damage = max((attack - defender['defense'], 0))
            defender['hp'] -= damage
            print(f'{attacker["name"]} attacks {defender["name"]} for {damage} damage, leaving with {defender["hp"]} health left')
        else:
            print(f'{attacker["name"]} cannot attack {defender["name"]} because they have no ranged attack')

    def is_dead(unit):
        return unit['hp'] <= 0

    player = make_unit(
        name='Player',
        hp=10,
        attack=4,
        defense=2,
    )

    enemies = [
        make_unit(
            name='Goblin',
            hp=5,
            attack=6,
            defense=1,
        ),
        make_unit(
            name='Bat',
            hp=5,
            attack=3,
            defense=0,
        ),
        make_unit(
            name='Snake',
            hp=5,
            attack=5,
            defense=1,
        ),
    ]

    while True:
        if is_dead(player):
            print(f'{player["name"]} has died.')
            break
        if not enemies:
            print(f'{player["name"]} has won.')
            break

        for enemy in enemies:
            attack(player, enemy)
            if is_dead(enemy):
                print(f'{enemy["name"]} has died.')
                enemies.remove(enemy)
            else:
                attack(enemy, player)

                if is_dead(player):
                    break

Now, the player dies, because they have no ranged attacks:

.. code-block:: text

  Player attacks Goblin for 3 damage, leaving with 2 health left
  Goblin attacks Player for 4 damage, leaving with 6 health left
  Player cannot attack Bat because they have no ranged attack
  Bat attacks Player for 1 damage, leaving with 5 health left
  Player attacks Snake for 3 damage, leaving with 2 health left
  Snake attacks Player for 3 damage, leaving with 2 health left
  Player attacks Goblin for 3 damage, leaving with -1 health left
  Goblin has died.
  Player attacks Snake for 3 damage, leaving with -1 health left
  Snake has died.
  Player cannot attack Bat because they have no ranged attack
  Bat attacks Player for 1 damage, leaving with 1 health left
  Player cannot attack Bat because they have no ranged attack
  Bat attacks Player for 1 damage, leaving with 0 health left
  Player has died.

If we add a weak ranged attack to player::
  
  player = make_unit(
      name='Player',
      hp=10,
      attack=4,
      ranged_attack=2,
      defense=2,
  )

Then the player barely wins:

.. code-block:: text
  
  Player attacks Goblin for 3 damage, leaving with 2 health left
  Goblin attacks Player for 4 damage, leaving with 6 health left
  Player attacks Bat for 2 damage, leaving with 3 health left
  Bat attacks Player for 1 damage, leaving with 5 health left
  Player attacks Snake for 3 damage, leaving with 2 health left
  Snake attacks Player for 3 damage, leaving with 2 health left
  Player attacks Goblin for 3 damage, leaving with -1 health left
  Goblin has died.
  Player attacks Snake for 3 damage, leaving with -1 health left
  Snake has died.
  Player attacks Bat for 2 damage, leaving with 1 health left
  Bat attacks Player for 1 damage, leaving with 1 health left
  Player attacks Bat for 2 damage, leaving with -1 health left
  Bat has died.
  Player has won.

But what happens when we want to add a new enemy, such as a Giant Mosquito?
We'd have to add another condition to attack function.  If we have dozens of
enemy types, we'd have to consider all the conditions for every one that has
special behaviors.  This is one of the purposes of python classes.

.. _typebehaviortags:

Type-specific behavior using tags, and custom attacking behavior
----------------------------------------------------------------

One other option that remains manageable without classes is a tag system, where
behaviors are tagged based on the unit in question.  This allows us to add a
Mosquito enemy.  We also want the mosquito enemy to ignore the player's defense
using their piercing attack, so we have to special-case this with another tag::

  def make_unit(name, hp, attack, defense, ranged_attack=None, tags=None):
      if tags is None:
          tags = set()

      return {
          'name': name,
          'hp': hp,
          'attack': attack,
          'ranged_attack': ranged_attack,
          'defense': defense,
          'tags': tags,
      }


  def attack(attacker, defender):
      if 'flying' in defender['tags']:
          attack = attacker['ranged_attack']
      else:
          attack = attacker['attack']

      if attack is not None:
          if 'piercing' in attacker['tags']:
              damage = attack
              print(f'{attacker["name"]} deals piercing damage, ignoring defense')
          else:
              damage = max((attack - defender['defense'], 0))
          defender['hp'] -= damage
          print(f'{attacker["name"]} attacks {defender["name"]} for {damage} damage, leaving with {defender["hp"]} health left')
      else:
          print(f'{attacker["name"]} cannot attack {defender["name"]} because they have no ranged attack')


  def is_dead(unit):
      return unit['hp'] <= 0


  player = make_unit(
      name='Player',
      hp=10,
      attack=4,
      ranged_attack=2,
      defense=2,
  )

  enemies = [
      make_unit(
          name='Goblin',
          hp=5,
          attack=6,
          defense=1,
      ),
      make_unit(
          name='Bat',
          hp=5,
          attack=3,
          defense=0,
          tags={'flying'},
      ),
      make_unit(
          name='Giant Mosquito',
          hp=5,
          attack=1,
          defense=0,
          tags={'flying', 'piercing'},
      ),
      make_unit(
          name='Snake',
          hp=5,
          attack=5,
          defense=1,
      ),
  ]

  while True:
      if is_dead(player):
          print(f'{player["name"]} has died.')
          break
      if not enemies:
          print(f'{player["name"]} has won.')
          break

      for enemy in enemies:
          attack(player, enemy)
          if is_dead(enemy):
              print(f'{enemy["name"]} has died.')
              enemies.remove(enemy)
          else:
              attack(enemy, player)

              if is_dead(player):
                  break

Using this kind of tag system, we can add behavior to units based on their tags.
This can be quite as powerful as you need, but isn't very flexible as you have
to have a big centralized function that encodes the behavior you want, and
modify that one regularly as you add more behaviors.  You can already see that
with only two special behaviors, the attack function is already getting more and
more convoluted.

.. code-block:: text

  Player attacks Goblin for 3 damage, leaving with 2 health left
  Goblin attacks Player for 4 damage, leaving with 6 health left
  Player attacks Bat for 2 damage, leaving with 3 health left
  Bat attacks Player for 1 damage, leaving with 5 health left
  Player attacks Giant Mosquito for 2 damage, leaving with 3 health left
  Giant Mosquito deals piercing damage, ignoring defense
  Giant Mosquito attacks Player for 1 damage, leaving with 4 health left
  Player attacks Snake for 3 damage, leaving with 2 health left
  Snake attacks Player for 3 damage, leaving with 1 health left
  Player attacks Goblin for 3 damage, leaving with -1 health left
  Goblin has died.
  Player attacks Giant Mosquito for 2 damage, leaving with 1 health left
  Giant Mosquito deals piercing damage, ignoring defense
  Giant Mosquito attacks Player for 1 damage, leaving with 0 health left
  Player has died.

Classes
=======

First, let's handle some definitions (simplified for beginners):

.. glossary::

  Object Oriented
    Just a way to specify code that uses :term:`classes <class>`.  It has some
    heavier academic implications, but the actual meaning of it varies a bit
    from language to language and in other contexts.  For the most part, it just
    means "Code where related data is grouped together and attached somehow to
    the code that uses it, usually through classes and :term:`methods
    <method>`".

  class
    A special construct in Python that holds mostly special functions called
    :term:`methods <method>`.  One of these :term:`methods <method>` is an
    optional :term:`constructor`.  Every :term:`method` takes an
    :term:`implicit` :term:`self` :term:`parameter`, and mostly operates on this
    :term:`self` :term:`object`.  Classes can use :term:`inheritance` for
    :term:`deduplication`.  Classes begin with an uppercase letter by
    convention.
    Often "class" and "type" are used to mean the same thing, and in the context
    of Python, they are the same thing.

  constructor
    The __init__ function inside a :term:`class`.  It can be left out if you
    don't need to add any attributes to the :term:`object` (often done if a
    class doesn't actually hold data, but is used strictly for its
    :term:`methods <method>`, or if the class is only intended to be
    :term:`inherited <inheritance>`).  Calling a :term:`class` like a function
    automatically calls the constructor.  Calling class ``Foo`` like ``x = Foo(arg,
    arg2)`` creates a blank ``Foo`` object and then calls the constructor with
    it as an argument.

  method
    A function inside a class with a first :term:`implicit` :term:`self`
    :term:`parameter`.

  parameter
    A function or :term:`method` argument as seen from inside the function.
    This is often used interchangeably with the word *argument*, but technically
    the argument is the value passed in, and the parameter is the name that it
    has inside the function.  Subtly different, but you can generally use either
    word and people will know what you're talking about, and almost nobody
    perfectly uses the right word in the right context.

  implicit
    Means that the :term:`parameter` does not need to be passed in as an
    argument.  The caller will automatically be passed in as the first argument.
    For instance, if you have :term:`class` ``Foo`` and call ``foo = Foo()`` and
    then ``foo.bar('test')`` which is defined as ``def bar(self, value):``, it's
    the same thing as calling ``foo = Foo()`` and then ``Foo.bar(foo,
    'value')``.  The first parameter is implied.

  object
    An *instance* of the :term:`class`.  This is the dictionary-like thing that
    is created by calling the class as a function, and it is associated with its
    class in a way that makes it so that when you call :term:`methods <method>`
    on it, they refer to its :term:`class` functions and pass it into them.
    Objects are put into variables that are all lowercase letters and
    underscores by convention.

  self
    The first parameter of a :term:`method`.  Doesn't actually need to be called
    ``self``, but should be.  This is not a magical or special object, and
    doesn't have any special semantics.  It is the exact object, you can access
    its attributes, set data on it, or call other methods with it (like
    ``self.other_method()``).

  inheritance
    A way of saying that a :term:`class` may be a *subclass* of another
    *superclass*, and :term:`objects <object>` of the subclass are also objects
    of the *superclass*, declared by putting the *superclass* in parentheses in
    the *subclass* declaration (such as ``class Subclass(Superclass):``).  This
    is mostly used as a method of :term:`deduplication`, but can serve more
    advanced purposes as well.

  deduplication
    The concept of trying to avoid having repetitive code that looks similar or
    the same.  There are many techniques to achieve this, and
    :term:`inheritance` is one of them.

  attribute
    A value stored on an :term:`object`.  Basically the exact same thing as keys
    in a dictionary, but accessed with a single period instead of brackets and
    quotes, so it's a bit less noisy, and there are more advanced features
    available to it.

Classes are another way of tackling this kind of task.  This can make things
much more readable and understandable, but can also make a lot of other things
easier as well.  One of the primary purposes of classes is that we can enforce
that an object is correctly built, where with dictionaries, anybody can create a
dictionary that can then fail to be processed with the functions that are
supposed to take it.  For instance if you simply pass an empty dictionary as a
parameter into the above ``attack`` functions, it will fail at runtime with some
annoying KeyErrors, but using a constructor, we can prevent calling a function
unless we have a valid object.

Here's a simple example of a class.  Functions inside the class take a ``self``
parameter (which can be named anything, but if you name it anything other than
``self``, everybody will get mad at you).  This is the *instance* of the class,
meaning the object that is created, and you can assign attributes to it.  You
need to assign attributes to it if you want to use them later.  Passing
parameters into the functions, including the constructor, does nothing magical,
and will not attach those parameters to the object.  They just go into the
function for the call, and will disappear afterward.  The constructor is
``__init__``, and also takes the first ``self`` parameter, and you can create an
*instance* of the class by just calling the class like a function::

  class Something:
      def __init__(self, alpha, beta, gamma):
          self.alpha = alpha
          self.second = beta
          self.third = 'hardcoded'

      def combined(self):
          return self.alpha + self.second

  foo = Something('foo', 'bar', 'baz') # self is implicit, and always comes first.  We don't pass self in ourselves.
  print(foo.alpha)      # prints foo
  print(foo.second)     # prints bar
  print(foo.third)      # prints hardcoded
  print(foo.combined()) # prints foobar
  print(foo.beta)       # ERROR, we never set self.beta to anything
  print(foo.gamma)      # ERROR, we never set self.gamma to anything

General behavior using classes
------------------------------

This is the first example :ref:`from above <generalbehavior>`, reworked with
classes::


  class Unit:
      def __init__(self, name, hp, attack, defense):
          self.name = name
          self.hp = hp
          self._attack = attack
          self.defense = defense

      def attack(self, defender):
          damage = max((self._attack - defender.defense, 0))
          defender.hp -= damage
          print(f'{self.name} attacks {defender.name} for {damage} damage, leaving with {defender.hp} health left')

      def is_dead(self):
          return self.hp <= 0

  player = Unit(
      name='Player',
      hp=10,
      attack=4,
      defense=2,
  )

  enemies = [
      Unit(
          name='Goblin',
          hp=5,
          attack=6,
          defense=1,
      ),
      Unit(
          name='Bat',
          hp=5,
          attack=3,
          defense=0,
      ),
      Unit(
          name='Snake',
          hp=5,
          attack=5,
          defense=1,
      ),
  ]

  while True:
      if player.is_dead():
          print(f'{player.name} has died.')
          break
      if not enemies:
          print(f'{player.name} has won.')
          break

      for enemy in enemies:
          player.attack(enemy)
          if enemy.is_dead():
              print(f'{enemy.name} has died.')
              enemies.remove(enemy)
          else:
              enemy.attack(player)

              if player.is_dead():
                  break

One thing that is noticeable is that when we assign ``attack`` to the unit
:term:`object` inside the :term:`constructor`, we prefix it with an underscore
(as ``_attack``).  In real :term:`Object Oriented` Python code, usually all of
an object's attributes are prefixed with underscores for other specific reasons
that we don't care about yet.  The reason we do it with ``attack`` is because it
will otherwise block the object from being able to access the ``attack``
:term:`method`, you will only be able to get that integer ``attack`` attribute,
because they have the exact same name.  Other than that, this code is very
similar to the code above, just replacing dictionary key access with object
attribute access, and function calls with method calls.

Type-specific behavior using overriding
---------------------------------------

Here we do something similar to :ref:`the tag example above <typebehaviortags>`,
but instead check if the enemy is a flying type, and use that to decide whether
we need flying attacks.  Here, we'll do it in two steps, doing just the
inheritance first, and then later we'll customize with piercing behavior in the
next example::

  class Unit:
      def __init__(self, name, hp, attack, defense, ranged_attack = None):
          self.name = name
          self.hp = hp
          self._attack = attack
          self.ranged_attack = ranged_attack
          self.defense = defense

      def attack(self, defender):
          attack = defender.attacker_offense(self)
          if attack is not None:
              damage = max((attack - defender.defense, 0))
              defender.hp -= damage
              print(f'{self.name} attacks {defender.name} for {damage} damage, leaving with {defender.hp} health left')
          else:
              print(f'{self.name} can not attack {defender.name}')

      def is_dead(self):
          return self.hp <= 0

      def attacker_offense(self, attacker):
          return attacker._attack

  class Flying(Unit):
      def attacker_offense(self, attacker):
          return attacker.ranged_attack

  player = Unit(
      name='Player',
      hp=10,
      attack=4,
      ranged_attack=2,
      defense=2,
  )

  enemies = [
      Unit(
          name='Goblin',
          hp=5,
          attack=6,
          defense=1,
      ),
      Flying(
          name='Bat',
          hp=5,
          attack=3,
          defense=0,
      ),
      Unit(
          name='Snake',
          hp=5,
          attack=5,
          defense=1,
      ),
  ]

  while True:
      if player.is_dead():
          print(f'{player.name} has died.')
          break
      if not enemies:
          print(f'{player.name} has won.')
          break

      for enemy in enemies:
          player.attack(enemy)
          if enemy.is_dead():
              print(f'{enemy.name} has died.')
              enemies.remove(enemy)
          else:
              enemy.attack(player)

              if player.is_dead():
                  break

Here we can see the inheritance in action.  The defending units now determine
their attacker's offense, using the ``attacker_offense`` method.  Using this, we
can have the Bat override the attacker's offense to get their ``ranged_attack``
instead.  This is one way of doing it, but not necessarily the best way.
Another way is by checking the type in the attack method (which looks a lot like
the previous tagged approach)::

    class Unit:
    ...
        def attack(self, defender):
            if isinstance(defender, Flying):
                attack = self.ranged_attack
            else:
                attack = self._attack

            if attack is not None:
                damage = max((attack - defender.defense, 0))
                defender.hp -= damage
                print(f'{self.name} attacks {defender.name} for {damage} damage, leaving with {defender.hp} health left')
            else:
                print(f'{self.name} can not attack {defender.name}')
    class Flying(Unit):
        pass

Type-specific behavior using type checking, and overriding attack behavior
--------------------------------------------------------------------------

Here we complete the :ref:`the tag example above <typebehaviortags>`, with the
Mosquito.  We also switch to the previously-mentioned type-checking approach,
but allow Flying units to attack other Flying units without ranged attacks (but
we don't use that behavior yet)::

  class Unit:
      def __init__(self, name, hp, attack, defense, ranged_attack = None):
          self.name = name
          self.hp = hp
          self._attack = attack
          self.ranged_attack = ranged_attack
          self.defense = defense

      def attack(self, defender):
          if isinstance(defender, Flying):
              attack = self.ranged_attack
          else:
              attack = self._attack

          if attack is not None:
              damage = max((attack - defender.defense, 0))
              defender.hp -= damage
              print(f'{self.name} attacks {defender.name} for {damage} damage, leaving with {defender.hp} health left')
          else:
              print(f'{self.name} can not attack {defender.name}')

      def is_dead(self):
          return self.hp <= 0

  class Flying(Unit):
      def attack(self, defender):
          damage = max((attack - defender.defense, 0))
          defender.hp -= damage
          print(f'{self.name} attacks {defender.name} for {damage} damage, leaving with {defender.hp} health left')

  class Mosquito(Flying):
      def attack(self, defender):
          damage = max((attack - defender.defense, 0))
          defender.hp -= damage
          print(f'{self.name} deals piercing damage, ignoring defense')
          print(f'{self.name} attacks {defender.name} for {damage} damage, leaving with {defender.hp} health left')

  player = Unit(
      name='Player',
      hp=10,
      attack=4,
      ranged_attack=2,
      defense=2,
  )

  enemies = [
      Unit(
          name='Goblin',
          hp=5,
          attack=6,
          defense=1,
      ),
      Flying(
          name='Bat',
          hp=5,
          attack=3,
          defense=0,
      ),
      Mosquito(
          name='Giant Mosquito',
          hp=5,
          attack=1,
          defense=0,
      ),
      Unit(
          name='Snake',
          hp=5,
          attack=5,
          defense=1,
      ),
  ]

  while True:
      if player.is_dead():
          print(f'{player.name} has died.')
          break
      if not enemies:
          print(f'{player.name} has won.')
          break

      for enemy in enemies:
          player.attack(enemy)
          if enemy.is_dead():
              print(f'{enemy.name} has died.')
              enemies.remove(enemy)
          else:
              enemy.attack(player)

              if player.is_dead():
                  break

The Mosquito class has a customized attack method, allowing them to get their
own attack behavior.

Clearly, this could be organized a bit better, too.  We've eliminated a lot of
the ugly branching, and most of the behavior of attacking is encoded in the
types that are actually doing the attacking, but some things can be done better,
and we still have some repitition going on that can be further broken down to
prevent it and make things more readable and manageable.

For a user exercise, try to add a ``status`` attribute as a set to the ``Unit``
class, and allow the Snake to inflict the ``'poisoned'`` status on strike,
which deals one damage to a unit either each time they attack, or during a
``status_check()`` method that you add.  Note how the defender takes damage.
The status can be added in the same way.

.. _The Python Classes reference: https://docs.python.org/3/tutorial/classes.html
