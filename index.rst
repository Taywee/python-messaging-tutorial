.. Python Messaging Tutorial documentation master file, created by
   sphinx-quickstart on Wed Dec 16 09:40:32 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Python Class Tutorial, using Messaging
======================================

This is meant to be a simple low-impact introduction to classes in general,
using Python.  This set of examples uses a faux Instant Messenger to explain a
lot of the concepts, and for the most part, avoids inheritance, as many people
getting started with Python become quickly overwhelmed when looking into classes
and getting inundated with their more complex features and philosophies when
they have barely finished grappling with dictionaries in the first place.

This assumes that you have a grasp of simple python concepts, including strings,
numbers, functions, dictionaries, lists, and sets, and the next hole that is
lacking is classes.

The intention is that you start with the :ref:`classintro` part, and then use
the **Next** and **Previous** buttons to navigate the tutorial.

.. toctree::
  :maxdepth: 2
  :caption: Contents:

  classintro
  startingup
